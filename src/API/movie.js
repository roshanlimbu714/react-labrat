import axios from "axios";

export const APIGetMovies = ()=>{
    return axios.get('https://yts.mx/api/v2/list_movies.json');
}

export const APIGetMovieDetail = (id) => {
    return axios.get('https://yts.mx/api/v2/movie_details.json?movie_id='+id)
}