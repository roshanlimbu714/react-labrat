import logo from './logo.svg';
import './App.css';
import {Counter} from "./components/containers/Counter/Counter";
import {Home} from "./screens/Home";
import {MainLayout} from "./layouts/MainLayout";
import {Labrat} from "./screens/Labrat";

function App() {
  return (
    <div className="App">
      {/*<Counter/>*/}
      {/*  <MainLayout/>*/}
        <Labrat/>
    </div>
  );
}

export default App;
