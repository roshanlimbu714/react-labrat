import {useDispatch, useSelector} from "react-redux";
import counterReducer from "./Counter.reducer";
import * as actions from './Counter.actions';
export const Counter = () => {
    const dispatch = useDispatch();
    const count = useSelector(state=>state['counterReducer'].count);
    console.log(count)
    return (
        <>
            <h1>Count:{count}</h1>
            <button onClick={()=>dispatch(actions.increaseCounter(count+1))}>Increase Counter</button>
            <button onClick={()=>dispatch(actions.increaseCounter(count-1))}>Decrease Counter</button>
        </>
    )
}