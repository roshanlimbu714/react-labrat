import * as actionTypes from './Counter.types';

const initialState = {
    count: 0
}

const counterReducer = (state = initialState, actions) => {
    switch (actions.type) {
        case actionTypes.INCREASE_COUNT:
            return {
                ...state,
                count: actions.payload
            }
        case actionTypes.DECREASE_COUNT:
            return {
                ...state,
                count: actions.payload
            }
        default:
            return state;
    }
}

export default counterReducer;