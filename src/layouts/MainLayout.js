import {Route, Switch} from "react-router";
import {ROUTES} from "../routes";

export const MainLayout = ()=>{
    return (
        <Switch>
            {ROUTES.map((v,key)=>(
                <Route path={v.path} component={v.component} exact={v.exact} key={key}/>
            ))}
        </Switch>
    )
}