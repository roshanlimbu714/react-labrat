import {useDispatch, useSelector} from "react-redux";
import {useEffect} from "react";
import {useParams} from "react-router";
import {fetchMovie} from "../store/Movie/actions";


export const Detail = () => {
    const dispatch = useDispatch();
    const movie = useSelector(state => state['movieReducer'].movie);
    const movieID = useParams();
    useEffect(async () => {
        await getMovieDetail();
    }, []);

    const getMovieDetail = async () => {
        await dispatch(fetchMovie(movieID.id));
    }
    return (
        <section>
            <div>Movie detail</div>
            {movie.background_image && <img src={movie.background_image} alt=""/>}
            <h1>{movie.title ?? ''}</h1>
            <p>{movie.description_full ?? ''}</p>
        </section>
    );
}