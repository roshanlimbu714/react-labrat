import {useDispatch, useSelector} from "react-redux";
import {useEffect} from "react";
import * as actions from '../store/Movie/actions';
import {useHistory} from "react-router";
export const Home = ()=>{
    const dispatch = useDispatch();
    const movies = useSelector(state=>state['movieReducer'].movies);
    const history = useHistory();
    useEffect(()=>{
        getMovies();
    },[]);


    const getMovies = async () =>{
        await dispatch(actions.fetchMovies());
    }

    const changeRoute = (id)=>{
      history.push('/movie/'+id);
    }
    return (
        <section>
            <h1>Movie list</h1>
            {movies.map((v,key)=>
                <div onClick={()=>changeRoute(v.id)} key={key}>{v.title}</div>
            )}
        </section>
    )
}