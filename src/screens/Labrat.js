import {useState} from "react";

const INITIAL_USER = {
    name:'',
    phone:'',
    address:''
}
export const Labrat = () =>{
    const [ newVar, setVar] = useState(15);
    const increment=()=>{
        setVar(newVar+5)
    }

    const [userDetails, setUserDetails] = useState({...INITIAL_USER});
    const inputHandler=(e)=>{
       setUserDetails({
           ...userDetails,
           [e.target.name]:e.target.value
       });
    }
    return (
          <>
              <form>
                  <input type="text" placeholder='name' name='name' onChange={inputHandler}/>
                  <input type="text" placeholder='address' name='address'  onChange={inputHandler}/>
                  <input type="text" placeholder='phone' name='phone'  onChange={inputHandler}/>
              </form>

              <h1>Name: {userDetails.name} </h1>
              <h1>address: {userDetails.phone} </h1>
              <h1>phone: {userDetails.address} </h1>
          </>
    );
}