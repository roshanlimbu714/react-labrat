export const SET_MOVIES = "SET_MOVIES";
export const SET_MOVIE = "SET_MOVIE";
export const GET_MOVIE_DETAIL = "GET_MOVIE_DETAIL";