import * as actionTypes from './actionTypes';
import {APIGetMovieDetail, APIGetMovies} from "../../API/movie";

export const setMovies = (val) => {
    return {
        type: actionTypes.SET_MOVIES,
        payload: val
    }
}

export const setMovie = (val) => {
    return {
        type: actionTypes.SET_MOVIE,
        payload: val
    }
}

export const fetchMovies = () =>async dispatch => {
    let res = await APIGetMovies();
    await dispatch(setMovies(res['data'].data.movies));
}

export const fetchMovie = (id) =>async dispatch => {
    let res = await APIGetMovieDetail(id);
    console.log(res['data'].data.movie);
    await dispatch(setMovie(res['data'].data.movie));
}


