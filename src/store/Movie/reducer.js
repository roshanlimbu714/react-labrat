import * as actionTypes from './actionTypes';

const initialState = {
    movies: [],
    movie: {}
}

const movieReducer = (state = initialState, actions) => {
    switch (actions.type) {
        case actionTypes.SET_MOVIES:
            return {
                ...state,
                movies: actions.payload
            }
        case actionTypes.SET_MOVIE:
            return {
                ...state,
                movie: actions.payload
            }
        default:
            return state;
    }
}

export default movieReducer;