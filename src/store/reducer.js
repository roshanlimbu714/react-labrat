import {combineReducers} from "redux";
import counterReducer from '../components/containers/Counter/Counter.reducer';
import movieReducer from './Movie/reducer';

export default combineReducers({
    counterReducer,
    movieReducer
});